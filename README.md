# expects
A C++17 library to document and assert expectations. With hooks for user 
defined assertion functions and intelligent -fno-exceptions handling, provides
a flexible code footprint suitable for bare-metal applications.

Copyright(C) Jeremy Jurksztowicz 2020. All rights reserved.

### See expects.hpp for details and documentation.
