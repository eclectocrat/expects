/** Copyright(C) Jeremy Jurksztowicz 2020. All rights reserved.

    expects.hpp

    A basic boolean assertion function that is -fno-exceptions aware, along 
    with a few types for self-documenting parameters or fields.

    The primary motivation for this file was that assert() requires linking to
    abort(), which is not available on bare metal systems. This file provides
    an alternative with override hooks that can be utilized to provide custom
    assertion failure reporting, i.e. lighting an error LED on a hardware unit.
    For example:

        #define EC_UNEXPECTED(x) HAL_set_led_color(LED_ERROR, COLOR_RED)
        #include <expects.hpp>

    In addition to the core expects() function, three template classes are
    provided to assist in asserting expectations on parameters while 
    documenting to callers what those expectations are.

    Expects_nonzero<I>      e.g. void set_user_count(Expects_nonzero<size_t>)
    Expects_nonnull_ptr<T*> e.g. auto f = Expects_nonnull_ptr(get_file());
    Expects_in_range<I>     e.g. void set_age(Expects_in_range<size_t>(0, 99))
*/

#ifndef _ec_expects_hpp
#define _ec_expects_hpp

#include <type_traits>
#include <cstddef>

#ifdef __EXCEPTIONS
    #define EC_UNEXPECTED(x) throw x;
#else
    #ifndef EC_UNEXPECTED
    #define EC_UNEXPECTED(x) (void)x;
    #endif
#endif

namespace ec {

struct Unexpected {};

constexpr void expects(const bool test) {
    if (not test) { 
        EC_UNEXPECTED(Unexpected{})
    }
}

template<typename T, typename = 
    std::enable_if<std::is_integral<T>::value or std::is_pointer<T>::value>>
struct Expects_nonzero {
    template <typename U>
    constexpr Expects_nonzero(U const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(T const& v): _value(v) { expects(_value != 0); }
    constexpr Expects_nonzero(Expects_nonzero const&) = default;
    Expects_nonzero& operator=(Expects_nonzero const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
    template<typename U = T> 
    constexpr typename std::enable_if<std::is_pointer<U>::value, U>::type& 
        operator->() { return _value; }
    template<typename U = T> 
    constexpr typename std::enable_if<std::is_pointer<U>::value, U>::type const& 
        operator->() const { return _value; }
private:
    T _value = T();
};

template<typename T>
using Expects_nonnull_ptr = Expects_nonzero<T*>;

template<typename T, size_t Begin, size_t End,
         typename = std::enable_if<std::is_integral<T>::value>>
struct Expects_in_range {
    template <typename U>
    constexpr Expects_in_range(U const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_in_range(T const& v): _value(v) {
        expects(_value >= Begin);
        expects(_value < End);
    }
    constexpr Expects_in_range(Expects_in_range const&) = default;
    Expects_in_range& operator=(Expects_in_range const&) = default;
    constexpr operator T() const { return _value; }
    constexpr T value() const { return _value; }
    constexpr T& weak() { return _value; }
private:
    T _value = T();
};

}
#endif
